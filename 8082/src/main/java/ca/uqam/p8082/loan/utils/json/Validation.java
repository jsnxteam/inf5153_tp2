package ca.uqam.p8082.loan.utils.json;

import ca.uqam.p8082.loan.utils.json.JsonCapsule;
import ca.uqam.p8082.loan.utils.vocabulary.Vocabulary;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Validation {
    private static final String FORMAT_ERROR = "Format JSON Invalide : ";
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    
    private static Validation instance;
    
    public static Validation getInstance(){
        if(instance == null) instance = new Validation();
        return instance;
    }
    
    /**
     * Validate json input for a Loan and throws an exception if something is wrong.
     * @param jsonObject
     * @throws Exception 
     */
    public void validate(JsonCapsule jsonObject) throws Exception{
       validateExistence(jsonObject);
       validateNotEmpty(jsonObject);
       validateIsPositive(jsonObject);
       validateDate(jsonObject.getObject(Vocabulary.PARAM_DATE_STR));
       validateNumerals(jsonObject);
    }
    
    
    private void validateIsPositive(JsonCapsule jsonObject)throws Exception{
        double montantInitial = Double.parseDouble(jsonObject.getObject(Vocabulary.PARAM_INIT_AMOUNT).toString());
        int nombrePeriodes = Integer.parseInt(jsonObject.getObject(Vocabulary.PARAM_PERIODS).toString());
        double tauxPeriodique = Double.parseDouble(jsonObject.getObject(Vocabulary.PARAM_RATE).toString());
        if(montantInitial < 0.0) throw new Exception("Le montant initial ne peut pas être négatif");
        if(nombrePeriodes < 0) throw new Exception("Le nombre de periodes ne peut pas être négatif");
        if(tauxPeriodique < 0.0) throw new Exception("Le taux périodique ne peut pas être négatif");
    }
    
    private void validateNumerals(JsonCapsule jsonObject) throws Exception{
        try{
            int numberOfPayments = Integer.parseInt(jsonObject.getObject(Vocabulary.PARAM_PERIODS).toString());
        }
        catch(NumberFormatException e)
        {
            System.out.println("Le nombre de periode doit être un entier positif");
        }
        
        try{
            double monthlyRate = Double.parseDouble(jsonObject.getObject(Vocabulary.PARAM_RATE).toString());
        }
        catch(NumberFormatException e)
        {
            System.out.println("Le taux périodique doit être un nombre a virgule positif");
        }
    }
    
    private void validateExistence(JsonCapsule jsonObject) throws Exception{
         if(!jsonObject.keyExists(Vocabulary.PARAM_SCENARIO_STR)) 
            throw new Exception(FORMAT_ERROR + "La clé \"scenario\" est manquante");
        else if(!jsonObject.keyExists(Vocabulary.PARAM_DATE_STR)) 
            throw new Exception(FORMAT_ERROR + "La clé \"datePret\" est manquante");
        else if (!jsonObject.keyExists(Vocabulary.PARAM_INIT_AMOUNT)) 
            throw new Exception(FORMAT_ERROR + "La clé \"montantInitial\" est manquante");
        else if (!jsonObject.keyExists(Vocabulary.PARAM_PERIODS)) 
            throw new Exception(FORMAT_ERROR + "La clé \"nombrePeriodes\" est manquante");
        else if (!jsonObject.keyExists(Vocabulary.PARAM_RATE)) 
            throw new Exception(FORMAT_ERROR + "La clé \"tauxPeriodique\" est manquante");
         else if (!jsonObject.keyExists(Vocabulary.PAYMENT)) 
            throw new Exception(FORMAT_ERROR + "La clé \"versement\" est manquante");
    }
    
    private void validateNotEmpty(JsonCapsule jsonObject) throws Exception{
        if(jsonObject.getObject(Vocabulary.PARAM_SCENARIO_STR).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"scenario\" ne peut pas être vide");
        else if(jsonObject.getObject(Vocabulary.PARAM_DATE_STR).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"datePret\" ne peut pas être vide");
        else if(jsonObject.getObject(Vocabulary.PARAM_INIT_AMOUNT).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"montantInitial\" ne peut pas être vide");
        else if(jsonObject.getObject(Vocabulary.PARAM_PERIODS).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"nombrePeriodes\" ne peut pas être vide");
        else if(jsonObject.getObject(Vocabulary.PARAM_RATE).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"tauxPeriodique\" ne peut pas être vide");    
        else if(jsonObject.getObject(Vocabulary.PAYMENT).toString().isEmpty()) 
            throw new Exception(FORMAT_ERROR + "La valeur de \"versement\" ne peut pas être vide");    
    }
    
    private void validateDate(Object obj) throws Exception{   
        Calendar date;
        try{
            date = Calendar.getInstance();
            date.setTime(DATE_FORMATTER.parse(obj.toString()));
        }
        catch (ParseException e){
            System.out.println("Format de date invalide");
        } 
    }
    
    private Validation(){}
}
