package ca.uqam.p8082.loan.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class Formatter {
    private static Formatter instance;
    
    private NumberFormat frenchCanadian = NumberFormat.getCurrencyInstance(Locale.CANADA_FRENCH);
    
    public static Formatter getInstance(){
        if(instance == null) instance = new Formatter();
        return instance;
    }
   
    public String getCurrency(BigDecimal amount){
        String ret;
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            ret = frenchCanadian.format(amount.abs());
            ret = betweenParenthesis(ret);
        } else {
            ret = frenchCanadian.format(amount);
        }
        return ret;
    }
    
    private String betweenParenthesis(String s){
        return "("+s+")";
    }
}
