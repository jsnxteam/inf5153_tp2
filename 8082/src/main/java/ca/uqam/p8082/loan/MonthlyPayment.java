package ca.uqam.p8082.loan;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.math.BigDecimal;
import java.math.MathContext;


public class MonthlyPayment {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private String date;
    private BigDecimal startingBalance, interest, capital, endBalance, totalPaid, totalInterest, totalCapital, monthlyPayment;
    private int period;
    
    
    
    public MonthlyPayment(int period, String currentDate, BigDecimal currentBalance, BigDecimal interest, BigDecimal capital, BigDecimal monthlyPayment, BigDecimal endBalance, 
            BigDecimal totalPaid, BigDecimal totalInterest, BigDecimal totalCapital, MathContext mc){
        this.startingBalance = currentBalance.setScale(2, mc.getRoundingMode());
        this.date = currentDate;
        this.interest = interest.setScale(2, mc.getRoundingMode());
        this.capital = capital.setScale(2, mc.getRoundingMode());
        this.endBalance = endBalance.setScale(2, mc.getRoundingMode());
        this.monthlyPayment = monthlyPayment.setScale(2, mc.getRoundingMode());
        this.totalPaid = totalPaid.setScale(2, mc.getRoundingMode());
        this.totalInterest = totalInterest.setScale(2, mc.getRoundingMode());
        this.totalCapital = totalCapital.setScale(2, mc.getRoundingMode());
        this.period = period;
    }
    
    public String getDate() { return date; }
    public BigDecimal getStartingBalance() { return startingBalance; }
    public BigDecimal getInterest() { return interest; }
    public BigDecimal getCapital() { return capital; }
    public BigDecimal getEndBalance() { return endBalance; }
    public BigDecimal getTotalPaid(){ return totalPaid;}
    public BigDecimal getTotalInterest(){ return totalInterest; }
    public BigDecimal getTotalCapital(){ return totalCapital; }
    public BigDecimal getMonthlyPayment(){ return monthlyPayment; }
    public int getPeriod(){ return period; }
     
    public void setTotalPaid(BigDecimal cumul){ totalPaid = cumul;}
    public void setTotalInterest(BigDecimal cumul){ totalInterest = cumul;}
    public void setTotalCapital(BigDecimal cumul){ totalCapital = cumul;}
    
    
    
}

