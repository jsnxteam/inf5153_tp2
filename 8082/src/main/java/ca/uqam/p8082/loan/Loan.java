package ca.uqam.p8082.loan;

import ca.uqam.p8082.loan.utils.json.JsonInterface;
import ca.uqam.p8082.loan.utils.json.JsonCapsule;
import ca.uqam.p8082.loan.utils.vocabulary.Vocabulary;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Loan {
    
    private String scenario;
    private Calendar loanDate;
    private String printableDate;
    private BigDecimal initialAmount;
    private int numberOfPayments;
    private double monthlyRate;
    
    private BigDecimal monthlyPayment;
    private BigDecimal totalPaid = BigDecimal.ZERO;
    private BigDecimal totalInterest = BigDecimal.ZERO;
    private BigDecimal totalCapital = BigDecimal.ZERO;
    
    private RoundingMode rounding = RoundingMode.HALF_EVEN;
    private MathContext mc = new MathContext(16, rounding);
    
    private ArrayList<MonthlyPayment> monthlyPayments = new ArrayList();
    private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    
    public Loan(JsonCapsule capsule) throws ParseException, IOException{
        JsonInterface jsonInterface = JsonInterface.getInstance();
        Object parametersObj = jsonInterface.parseJsonFromString(capsule.getObject(Vocabulary.PARAMETERS).toString());
        Object calendarObj = jsonInterface.parseJsonFromString(capsule.getObject(Vocabulary.CALENDAR).toString());
        JsonCapsule parameters = new JsonCapsule((JSONObject) parametersObj);
        
        this.scenario = parameters.getObject(Vocabulary.PARAM_SCENARIO_STR).toString();
        this.numberOfPayments =  Integer.parseInt(parameters.getObject(Vocabulary.PARAM_PERIODS).toString());
        this.printableDate =  parameters.getObject(Vocabulary.PARAM_DATE_STR).toString();
        this.initialAmount = new BigDecimal(parameters.getObject(Vocabulary.PARAM_INIT_AMOUNT).toString());
        this.monthlyRate = Double.parseDouble(parameters.getObject(Vocabulary.PARAM_RATE).toString());
        this.monthlyPayment = new BigDecimal(Double.parseDouble(parameters.getObject(Vocabulary.PAYMENT).toString())).setScale(2, rounding);
        this.monthlyPayments = initMonthlyPayments((JSONArray) calendarObj);
        
        this.totalPaid = this.monthlyPayments.get(this.monthlyPayments.size()-1).getTotalPaid();
        this.totalInterest = this.monthlyPayments.get(this.monthlyPayments.size()-1).getTotalInterest();
        this.totalCapital = this.monthlyPayments.get(this.monthlyPayments.size()-1).getTotalCapital();
    }
    
    private ArrayList<JsonCapsule> initCapsulesArray(JSONArray Object){
        ArrayList<JsonCapsule> capsules = new ArrayList();
        for(int i = 0; i < this.numberOfPayments; ++i){
            capsules.add(new JsonCapsule((JSONObject)Object.get(i)));
        }
        return capsules;
    }
    private ArrayList<MonthlyPayment> initMonthlyPayments(JSONArray Object){
        ArrayList<JsonCapsule> capsules = initCapsulesArray(Object);
        ArrayList<MonthlyPayment> arr = new ArrayList();
        int tempPeriod;
        String tempDate;
        BigDecimal tempCurrentBalance, tempInterest, tempCapital, tempMonthlyPayment, tempEndBalance, 
                   tempTotalPaid, tempTotalInterest, tempTotalCapital;
   
        for(JsonCapsule current : capsules)
        {
            tempPeriod = Integer.parseInt(current.getObject(Vocabulary.PERIOD).toString());
            tempDate = current.getObject(Vocabulary.PAYMENT_DATE).toString();
            tempCurrentBalance = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CAPITAL_B).toString()));
            tempInterest = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.INTEREST).toString()));
            tempCapital = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CAPITAL).toString()));
            tempMonthlyPayment = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.PAYMENT).toString()));
            tempEndBalance = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CAPITAL_E).toString()));
            tempTotalPaid = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CUMULATIVE_TOTAL).toString()));
            tempTotalInterest = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CUMULATIVE_INTEREST).toString()));
            tempTotalCapital = new BigDecimal(Double.parseDouble(current.getObject(Vocabulary.CUMULATIVE_CAPITAL).toString()));
       
            arr.add(new MonthlyPayment(tempPeriod, tempDate, tempCurrentBalance, tempInterest, tempCapital, tempMonthlyPayment, tempEndBalance, tempTotalPaid, tempTotalInterest, tempTotalCapital, mc));
        }
        return arr;
    }
    
    public ArrayList<MonthlyPayment> getMonthlyPayments(){ return this.monthlyPayments; }
    public String getScenario(){return scenario;}
    public String getLoanDate(){return this.printableDate;}
    public int getNumberOfPayments(){return numberOfPayments; }
    public double getMonthlyRate(){return monthlyRate; }
    public BigDecimal getInitialAmount() {return initialAmount;}
    public BigDecimal getMonthlyPayment() {return monthlyPayment;}
    public BigDecimal getTotalPaid() {return totalPaid;}
    public BigDecimal getTotalInterest() {return totalInterest;}
    public BigDecimal getTotalCapital() {return totalCapital;}
}

