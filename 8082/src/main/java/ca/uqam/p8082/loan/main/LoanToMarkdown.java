/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uqam.p8082.loan.main;

import ca.uqam.p8082.loan.utils.Formatter;
import ca.uqam.p8082.loan.Loan;
import ca.uqam.p8082.loan.MonthlyPayment;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Sayajin
 */
public class LoanToMarkdown {
    private static Formatter format = Formatter.getInstance();
    public static void generateMarkdown(Loan printable) {
        try{
            PrintWriter writer = new PrintWriter("output.md", "UTF-8");
            writer.println(createMarkdownStructure(printable));
            writer.close();
        } catch (IOException e) {
        }
    }   
    private static String generateTable(Loan loan){
        String ret;
        
        ret = "# Calendrier des remboursements\n\n"
            + "| **No** | **Date** | **Solde début** | **Versement** | **Intérêt** | **Capital** | **Solde fin** |\n"
            + "|:---|:----------:|------------:|----------:|----------:|----------:|------------:|\n";
        for( MonthlyPayment it : loan.getMonthlyPayments()){
            ret += "|"+it.getPeriod()+"|"+it.getDate()+"|"+format.getCurrency(it.getStartingBalance())+"|"+format.getCurrency(it.getMonthlyPayment())+"|"+
                    format.getCurrency(it.getInterest())+"|"+format.getCurrency(it.getCapital())+"|"+format.getCurrency(it.getEndBalance())+"|\n";
        }
        return ret;
    }
    private static String generateParameters(Loan loan){
        return  "# Paramètres du prêt\n\n"
            +"Scénario : **"+loan.getScenario()+"**\n\n"
            + "Date du prêt : **"+loan.getLoanDate()+"**\n\n"
            + "Montant initial du prêt : **"+format.getCurrency(loan.getInitialAmount())+"**\n\n"
            + "Nombre de mensualités : **"+loan.getNumberOfPayments()+"**\n\n"
            + "Taux mensuel : **"+(int)(loan.getMonthlyRate()*100)+" %**\n\n"
            + "Versement mensuel : **"+format.getCurrency(loan.getMonthlyPayment())+"**\n\n";
    }
    private static String generateTotals(Loan loan){
        return "# Totaux\n\n"
            + "Total des paiements : ***"+format.getCurrency(loan.getTotalPaid())+"***\n\n"
            + "Total des intérêts : ***"+format.getCurrency(loan.getTotalInterest())+"***\n\n"
            + "Total du capital remboursé : ***"+format.getCurrency(loan.getTotalCapital())+"***";
    }
    private static String createMarkdownStructure(Loan loan)
    {
        String ret, header = "% Calcul du remboursement d'un prêt\n"
            + "% Jean-Sébastien Paul et Nicolas Hamard -- INF5153 -- UQAM\n"
            + "% \\today\n\n"
            + "---\n"
            + "header-includes:\n"
            + "\t- \\usepackage{floatrow}\n"
            + "\t- \\DeclareFloatFont{normalsize}{\\normalsize}\n"
            + "\t- \\floatsetup[table]{font=normalsize}\n"
            + "---\n\n";
        ret = header
            + generateParameters(loan)
            + generateTable(loan)
            + "\n"
            + generateTotals(loan);
        
        return ret;
    }
    
}

