/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uqam.p8082.loan.main;

import ca.uqam.p8082.loan.Loan;
import ca.uqam.p8082.loan.utils.json.JsonInterface;
import ca.uqam.p8082.loan.utils.json.JsonCapsule;
import java.io.IOException;
import java.text.ParseException;
import org.json.simple.JSONObject;

/**
 *
 * @author Sayajin
 */
public class App {
     public static void main(String [] args) throws IOException, ParseException, Exception{
        if(args.length < 1) throw new Exception("Pas assez de parametres.");
        
        JsonInterface jsonInterface = JsonInterface.getInstance();
        JsonCapsule capsule = new JsonCapsule((JSONObject) jsonInterface.parseJson(args[0]));
        
     /*   Validation validator = Validation.getInstance();
        validator.validate(capsule);*/
       
        Loan loan = new Loan(capsule);
        LoanToMarkdown.generateMarkdown(loan);
       
    }
}
