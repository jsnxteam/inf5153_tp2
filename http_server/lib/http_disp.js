//requires
var http = require("http"),
    winston = require("winston"),
    logger = new winston.Logger({
        level: 'info',
        transports: [
            new (winston.transports.Console)({
                level: 'silly',
                timestamp: function(){
                    var now = new Date(Date.now());
                    return now.toISOString();
                },
                formatter: function(options){
                    return options.timestamp() +' ['+ options.level.toUpperCase() +'] '+ (options.message ? options.message : ''); 
                }
            }),
            new (winston.transports.File)({ filename : 'server.log' })
        ]
    });

//MIcroservices configuration--------------------------
var host808x = ["localhost", "localhost", "localhost"];
var path808x = ["/ms_8080", "/ms_8081", "/ms_8082"];
//-----------------------------------------------------

module.exports = function(request, callback){
    logger.log('info','New HTTP request from client, sending work to microservices...');
    service8080(JSON.stringify(request.body), function(result){
        service8081(result, function(result){
            service8082(result, function(result){
                callback(result);
                logger.log('info', 'Sending response to client...');
            });
        });

    });
}

function service8080(jsonOBJ, callback){
    logger.log('silly', 'Sending work to service 8080 via POST...');
    httpRequest(host808x[0], path808x[0], '8080', 'POST', jsonOBJ, function(result){
        callback(result);
    });
}

function service8081(jsonOBJ, callback){
    logger.log('silly', 'Sending work to service 8081 via POST...');
    httpRequest(host808x[1], path808x[1],'8081', 'POST', jsonOBJ, function(result){
        callback(result);
    });
}

function service8082(jsonOBJ, callback){
    logger.log('silly','Sending work to service 8082 via POST...');
    httpRequest(host808x[2], path808x[2],'8082', 'POST', jsonOBJ, function(result){
        callback(result);
    });
}

function httpRequest(HOST, PATH, PORT, METHOD, CONTENT, callback){
    var options = {
        host: HOST,
        port: PORT,
        path: PATH,
        method: METHOD,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(CONTENT)
        }
    };

    var httpreq = http.request(options, function(response){
        logger.log('info', 'Sending HTTP request to '+HOST+PATH+' on '+PORT+' via '+METHOD);
        response.setEncoding('utf8');
        var body = '';
        response.on('data', function(chunk){
            body += chunk;
        });
        response.on('end', function(){
            logger.log('info', HOST+PATH+':'+PORT+ 'success. Please check server.log for complete data review', {data: body});
            callback(body); 
        });
        response.on('error', function(e){
            logger.log('error', e.message);
        });
    });
    
    httpreq.write(CONTENT);
    httpreq.end();
}
