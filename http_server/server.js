//requires
var express = require("express"),
    bodyParser = require("body-parser"),
    showdown = require("showdown"),
    mdConverter = new showdown.Converter(),
    index = require("./lib/http_disp"),
    fs = require("fs"),
    server = express(),
    port = process.env.port || 8079,
    hostname = 'localhost';

server.url = "http://"+hostname;

//POST request handling
server.use(bodyParser.json()); //support json bodies
server.use(bodyParser.urlencoded({ extended : true })); //support encoded bodies

server.get('/index', function(req, res, cb){
    var index = fs.readFileSync('./lib/index.html');
    res.writeHeader(200, {"Content-type":"text/html"});
    res.write(index);
    res.end();
    return cb();
});

mdConverter.setFlavor("github");
mdConverter.setOption("tables", true);

//listen on http://hostname/PATH
server.post('/calendar', function(req, res, cb){
    index(req, function(result){
        var html_result = mdConverter.makeHtml(result);
        html_result = html_result.replace(/\<p[^.]*h2\>/gm, "");
        res.writeHeader(200, {"Content-type":"text/html"});
        res.write(html_result);
        res.end();
        return cb();
    })
});

//bind server on port 
server.listen(port, function(){
    console.log('%s listening at %s:%s', server.name, server.url, port);
})
