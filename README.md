[comment]: <> (Please visit https://bitbucket.org/jsnxteam/inf5153_tp2/overview if you want the readme in proper markdown display.)

#README#
========
##INTRODUCTION##
Ce projet a ete realise avec Node.js et Java. Il permet de produire un calendrier de remboursement de pret a partir d'une entree.

Il contient 4 services dont :

* un serveur HTTP dans `http_server/`
* un microservice effectuant les calculs preliminaires dans `8080/`
* un microservice sortant le calendrier de remboursement dans `8081/`
* un microservice delivrant une sortie en markdown dans `8082/`

Chaque microservice contient son serveur HTTP dans le dossier `[microservice]/http_receiver/`. Les fichiers json intermediaires passes entre les 3 microservices sont disponnibles localement dans chacun de leur repertoire respectifs (nomme `[microservice]/http_receiver/output.[json/md]`) ou dans les logs du servers a `http_server/server.log`.

##INSTALLATION##
Si vous venez de telecharger le projet, voici la procedure a suivre pour le demarrer :
Depuis la __racine du projet__ :

* `npm run update_all`
* `npm start`

L'entree du projet se trouve par defaut ici : [http://localhost:8079/index](http://localhost:8079/index)


Quelques commandes pour gerer l'environnement __A EXECUTER DEPUIS LA RACINE DU PROJET__:

* `npm run update_all` : installe/mets a jours les libraries de tous les serveurs Node.js
* `npm start` : lance le serveur http ainsi que les 3 micro-services
* `npm run start_server` : lance le serveur http
* `npm run start_8080` : lance le microservice du port 8080
* `npm run start_8081` : lance le microservice du port 8081
* `npm run start_8082` : lance le microservice du port 8082

Pour lancer les services Node.js directement depuis leurs dossiers respectifs, placez vous dans le repertoire du service et lancez `npm start`.
Bien que les jars de chaque microservices sont deja fourni dans le dossier `jars`, les source des projets java sont fourni avec un pom.xml pour les builds via maven.

##Architecture##
Voici un schema qui resume l'architecture du projet :
![UML/Architecture.png](https://bytebucket.org/jsnxteam/inf5153_tp2/raw/631c675baa6e3a75294bb34fe916a1f7af08474c/UML/Architecture.png?token=769aa7bb2e8827cb37c6d95f02bb2ddf8bb94327)
