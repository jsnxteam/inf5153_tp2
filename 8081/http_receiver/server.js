//requires
var express = require("express"),
    bodyParser = require("body-parser"),
    index = require("./lib/index"),
    path = require("path"),
    server = express(),
    port = process.env.port || 8081,
    hostname = 'localhost',
    PATH = '/ms_8081';

server.url = "http://"+hostname+PATH;

//POST request handling
server.use(bodyParser.json()); //support json bodies
server.use(bodyParser.urlencoded({ extended : true })); //support encoded bodies

//listen on http://localhost
server.post(PATH, function(req, res, cb){
    var jarPath = path.resolve('./jars/8081.jar');
    console.log("[From"+port+"]"+jarPath);
    res.send(index(req, jarPath)); //call request dispatcher
    return cb();
});


//bind server on port 
server.listen(port, function(){
    console.log('%s listening at %s:%s', server.name, server.url, port);
});
