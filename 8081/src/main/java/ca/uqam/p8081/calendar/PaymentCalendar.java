/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uqam.p8081.calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ca.uqam.p8081.calendar.utils.json.JsonCapsule;
import ca.uqam.p8081.calendar.utils.vocabulary.Vocabulary;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class PaymentCalendar {
    
    private String scenario;
    private Calendar loanDate;
    private String printableDate;
    private BigDecimal initialAmount;
    private int numberOfPayments;
    private double monthlyRate;
    
    private BigDecimal monthlyPayment;
    private BigDecimal totalPaid = BigDecimal.ZERO;
    private BigDecimal totalInterest = BigDecimal.ZERO;
    private BigDecimal totalCapital = BigDecimal.ZERO;
    
    private RoundingMode rounding = RoundingMode.HALF_EVEN;
    private MathContext mc = new MathContext(16, rounding);
    
    private ArrayList<MonthlyPayment> monthlyPayments = new ArrayList();
    private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    
    public PaymentCalendar(JsonCapsule capsule) throws ParseException{
        this.scenario = capsule.getObject(Vocabulary.PARAM_SCENARIO_STR).toString();
        this.numberOfPayments =  Integer.parseInt(capsule.getObject(Vocabulary.PARAM_PERIODS).toString());
        this.printableDate =  capsule.getObject(Vocabulary.PARAM_DATE_STR).toString();
        initDatePret(capsule.getObject(Vocabulary.PARAM_DATE_STR).toString());
        this.initialAmount = new BigDecimal(capsule.getObject(Vocabulary.PARAM_INIT_AMOUNT).toString()).setScale(2, rounding);
        this.monthlyRate = Double.parseDouble(capsule.getObject(Vocabulary.PARAM_RATE).toString());
        this.monthlyPayment = new BigDecimal(Double.parseDouble(capsule.getObject(Vocabulary.PAYMENT).toString())).setScale(2,rounding);
    }
    public JSONObject toJsonObject(){
        JSONObject ret = new JSONObject();
        
        ret.put(Vocabulary.PARAM_DATE_STR, this.printableDate);
        ret.put(Vocabulary.PARAM_INIT_AMOUNT, this.initialAmount);
        ret.put(Vocabulary.PARAM_PERIODS, this.numberOfPayments);
        ret.put(Vocabulary.PARAM_SCENARIO_STR, this.scenario);
        ret.put(Vocabulary.PARAM_RATE, this.monthlyRate);
        ret.put(Vocabulary.PAYMENT, this.monthlyPayment);
        return ret;
    }
    
    public JSONArray buildMonthlyPaymentJson()
    {
        JSONArray payments = new JSONArray();
        JSONObject temp;
        int period = 1;

        for(MonthlyPayment payment : monthlyPayments)
        {
            temp = new JSONObject();
            temp.put(Vocabulary.PERIOD, period);
            temp.put(Vocabulary.PAYMENT_DATE, payment.getDate());
            temp.put(Vocabulary.CAPITAL_B, payment.getStartingBalance());
            temp.put(Vocabulary.PAYMENT, this.monthlyPayment);
            temp.put(Vocabulary.INTEREST, payment.getInterest());
            temp.put(Vocabulary.CAPITAL, payment.getCapital());
            temp.put(Vocabulary.CAPITAL_E, payment.getEndBalance());
            temp.put(Vocabulary.CUMULATIVE_TOTAL, payment.getTotalPaid());
            temp.put(Vocabulary.CUMULATIVE_INTEREST, payment.getTotalInterest());
            temp.put(Vocabulary.CUMULATIVE_CAPITAL, payment.getTotalCapital());
            
            payments.add(temp);
            period++;
        }
        
       return payments;
    }
    private void initDatePret(String bruteDate) throws ParseException{
        try{
            this.loanDate = Calendar.getInstance();
            this.loanDate.setTime(DATE_FORMATTER.parse(bruteDate));
        } catch (ParseException e){
            System.out.println("Date format incorrect.");
        }
    }
    
    public void processRefunds(){
        MonthlyPayment payment;
        BigDecimal currentBalance = this.initialAmount;
        
        Calendar currentDate = this.loanDate;
        for(int i = 0; i< this.numberOfPayments; i++){
            currentDate.add(Calendar.MONTH, 1);
            payment = new MonthlyPayment (currentBalance, currentDate, this.monthlyRate, this.monthlyPayment, mc);
            addCapital(this.monthlyPayment, payment.getInterest(), payment.getCapital());
            currentBalance = payment.getEndBalance();
            payment.setTotalPaid(this.totalPaid);
            payment.setTotalInterest(this.totalInterest);
            payment.setTotalCapital(this.totalCapital);
            
            this.monthlyPayments.add(payment);
        }
    }
         
    public void addCapital(BigDecimal monthlyPayment, BigDecimal interest, BigDecimal capital){
        this.totalPaid = totalPaid.add(monthlyPayment);
        this.totalInterest = this.totalInterest.add(interest);
        this.totalCapital = this.totalCapital.add(capital);
    }
    
    public ArrayList<MonthlyPayment> getMonthlyPayments(){ return this.monthlyPayments; }
    public String getScenario(){return scenario;}
    public String getLoanDate(){return this.printableDate;}
    public int getNumberOfPayments(){return numberOfPayments; }
    public double getMonthlyRate(){return monthlyRate; }
    public BigDecimal getInitialAmount() {return initialAmount;}
    public BigDecimal getMonthlyPayment() {return monthlyPayment;}
    public BigDecimal getTotalPaid() {return totalPaid;}
    public BigDecimal getTotalInterest() {return totalInterest;}
    public BigDecimal getTotalCapital() {return totalCapital;}
}

