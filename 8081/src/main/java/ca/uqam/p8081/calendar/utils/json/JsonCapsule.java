package ca.uqam.p8081.calendar.utils.json;


import java.util.Iterator;
import java.util.Set;
import org.json.simple.JSONObject;

public class JsonCapsule { //credit Giulia
    private Set keys;
    private Object[] values;
    private int tokenIndex, size;
    private Iterator itKeys;
    
    /**
     * Construct from a JSONObject. Overlaps the JSONObject methods so all elements can be accessed either by keys or index.
     * @param data raw JSONObject
     */
    public JsonCapsule(JSONObject data) {
        this.tokenIndex = 0;
        this.keys = data.keySet();
        this.values = data.values().toArray();
        this.size = this.values.length;
        this.itKeys = this.keys.iterator();
    }
    
    /**
     * Size getter
     * @return number of JSONObject elements in the collection.
     */
    public int size() {
        return this.size;
    }
    
    /**
     * Whether or not a key string exists in the collection
     * @param key String to look for in the Set of keys
     * @return true if there is a match
     */
    public boolean keyExists(String key) {
        return getIndexOf(key) != -1;
    }
    
    /**
     * Get Object using String key to find it
     * @param key to look for
     * @return Object linked to the key
     */
    public Object getObject(String key) {
        if (getIndexOf(key) != -1)
            return this.values[getIndexOf(key)].toString();
        else
            return null;
    }
    
    /**
     * Get Object using index to find it
     * @param index
     * @return Corresponding Object
     */
    public Object getObject(int index) {
        return this.values[index];
    }
    
    /**
     * Get the Key using index to find it
     * @param index
     * @return Corresponding Key
     */
    public Object getKey(int index) {
        int i = 0;
        for (Iterator iterator = this.keys.iterator(); iterator.hasNext();) {
            Object next = iterator.next();
            if (i == index)
                return next;
            i++;
        }
        return null;
    }
    
    //protected constructor for token purpose only.
     protected JsonCapsule(Set keys, Object[] values){
        this.tokenIndex = 0;
        this.keys = keys;
        this.values = values;
        this.size = this.values.length;
    }
     
     private int getIndexOf(String key) {
        int i = 0;
        for (Iterator iterator = this.keys.iterator(); iterator.hasNext();) {
            if (iterator.next().toString().equals(key))
                return i;
            i++;
        }
        return -1;
    }
}
