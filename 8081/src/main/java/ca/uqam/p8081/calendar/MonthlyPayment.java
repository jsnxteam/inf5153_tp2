package ca.uqam.p8081.calendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.math.BigDecimal;
import java.math.MathContext;


public class MonthlyPayment {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private String date;
    private BigDecimal startingBalance, interest, capital, endBalance, totalPaid, totalInterest, totalCapital;
    
    
    
    public MonthlyPayment(BigDecimal currentBalance, Calendar currentDate, double monthlyRate, BigDecimal monthlyPayment, MathContext mc){
        startingBalance = currentBalance;
        date = formatter.format(currentDate.getTime());
        interest = currentBalance.multiply(new BigDecimal(monthlyRate), mc).setScale(2, mc.getRoundingMode());
        capital = monthlyPayment.subtract(interest, mc).setScale(2, mc.getRoundingMode());
        endBalance = currentBalance.subtract(capital, mc).setScale(2, mc.getRoundingMode());     
    }
    
    public String getDate() { return date; }
    public BigDecimal getStartingBalance() { return startingBalance; }
    public BigDecimal getInterest() { return interest; }
    public BigDecimal getCapital() { return capital; }
    public BigDecimal getEndBalance() { return endBalance; }
    public BigDecimal getTotalPaid(){ return totalPaid;}
    public BigDecimal getTotalInterest(){ return totalInterest; }
    public BigDecimal getTotalCapital(){ return totalCapital; }
    
    public void setTotalPaid(BigDecimal cumul){ totalPaid = cumul;}
    public void setTotalInterest(BigDecimal cumul){ totalInterest = cumul;}
    public void setTotalCapital(BigDecimal cumul){ totalCapital = cumul;}
    
    
    
}

