/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uqam.p8081.calendar.main;

import ca.uqam.p8081.calendar.utils.json.JsonCapsule;
import ca.uqam.p8081.calendar.utils.json.JsonInterface;
import ca.uqam.p8081.calendar.PaymentCalendar;
import ca.uqam.p8081.calendar.utils.json.Validation;
import java.io.IOException;
import java.text.ParseException;
import org.json.simple.JSONObject;

/**
 *
 * @author Sayajin
 */
public class App {
     public static void main(String [] args) throws IOException, ParseException, Exception{
        if(args.length < 1) throw new Exception("Pas assez de parametres.");
        
        JsonInterface jsonInterface = JsonInterface.getInstance();
        JsonCapsule capsule = new JsonCapsule((JSONObject) jsonInterface.parseJson(args[0]));
        JSONObject output = new JSONObject();
        
        Validation validator = Validation.getInstance();
        validator.validate(capsule);
       
        PaymentCalendar calendar = new PaymentCalendar(capsule);
        calendar.processRefunds();
        output.put("parametres",calendar.toJsonObject());
        output.put("calendrier", calendar.buildMonthlyPaymentJson());
        jsonInterface.outputWriter(output, "output.json");      
    }
}
