var http = require("http"),
    fs = require("fs"),
    path = require("path"),
    exec = require("child_process").execSync;

module.exports = function(request, pathToJar){
    //read json input from request
    var jsonString = JSON.stringify(request.body);
    fs.writeFileSync("input.json", jsonString)
    var inputFile = path.resolve('input.json');
    
    //execute jar
    var cmd = "java -jar "+pathToJar+" "+inputFile;
    exec(cmd);

    //read output from jar
    var outputFile = path.resolve('output.json');
    var outputJson = '';
    
    outputJson = fs.readFileSync(outputFile)     
    //return as http response.
    return(outputJson);
}
