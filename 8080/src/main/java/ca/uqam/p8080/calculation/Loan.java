/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uqam.p8080.calculation;

import ca.uqam.p8080.calculation.utils.json.JsonCapsule;
import ca.uqam.p8080.calculation.utils.vocabulary.Vocabulary;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import org.json.simple.JSONObject;

public class Loan {
    
    private String scenario;
    private Calendar loanDate;
    private String printableDate;
    private BigDecimal initialAmount;
    private int numberOfPayments;
    private double monthlyRate;
    
    private BigDecimal monthlyPayment;
    
    private RoundingMode rounding = RoundingMode.HALF_EVEN;
    private MathContext mc = new MathContext(16, rounding);
    
    private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    
    public Loan(JsonCapsule capsule) throws ParseException{
        this.scenario = capsule.getObject(Vocabulary.PARAM_SCENARIO_STR).toString();
        this.numberOfPayments =  Integer.parseInt(capsule.getObject(Vocabulary.PARAM_PERIODS).toString());
        this.printableDate =  capsule.getObject(Vocabulary.PARAM_DATE_STR).toString();
        initDatePret(capsule.getObject(Vocabulary.PARAM_DATE_STR).toString());
        this.initialAmount = new BigDecimal(capsule.getObject(Vocabulary.PARAM_INIT_AMOUNT).toString()).setScale(2, rounding);
        this.monthlyRate = Double.parseDouble(capsule.getObject(Vocabulary.PARAM_RATE).toString());
        this.monthlyPayment = calculateMonthlyPayment();
 
    }
    public JSONObject toJsonObject(){
        JSONObject ret = new JSONObject();
        String strdate = DATE_FORMATTER.format(this.loanDate.getTime());
        ret.put(Vocabulary.PARAM_DATE_STR, strdate);
        ret.put(Vocabulary.PARAM_INIT_AMOUNT, this.initialAmount);
        ret.put(Vocabulary.PARAM_PERIODS, this.numberOfPayments);
        ret.put(Vocabulary.PARAM_SCENARIO_STR, this.scenario);
        ret.put(Vocabulary.PARAM_RATE, this.monthlyRate);
        ret.put(Vocabulary.PAYMENT, this.monthlyPayment.setScale(2, rounding));
        return ret;
    }
    private void initDatePret(String bruteDate) throws ParseException{
        try{
            this.loanDate = Calendar.getInstance();
            this.loanDate.setTime(DATE_FORMATTER.parse(bruteDate));
        } catch (ParseException e){
            System.out.println("Date format incorrect.");
        }
    }
    
    private BigDecimal calculateMonthlyPayment(){
        double divisor = 1-(1/(Math.pow((1+monthlyRate), numberOfPayments)));
        BigDecimal ret = initialAmount.multiply(new BigDecimal(monthlyRate), mc).setScale(2, rounding);
        ret = ret.divide(new BigDecimal(divisor), mc);
        return ret;
    }
    
    public String getScenario(){return scenario;}
    public String getLoanDate(){return this.printableDate;}
    public int getNumberOfPayments(){return numberOfPayments; }
    public double getMonthlyRate(){return monthlyRate; }
    public BigDecimal getInitialAmount() {return initialAmount;}
    public BigDecimal getMonthlyPayment() {return monthlyPayment;}
}
