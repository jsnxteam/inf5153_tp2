package ca.uqam.p8080.calculation.utils.vocabulary;

public class Vocabulary {
    
    // Main Capsule Strings
    public static String PARAMETERS = "parametres";
    public static String CALENDAR = "calendrier";
    // Parameters Strings
    public static String PARAM_DATE_STR = "datePret";
    public static String PARAM_INIT_AMOUNT = "montantInitial";
    public static String PARAM_PERIODS = "nombrePeriodes";
    public static String PARAM_SCENARIO_STR = "scenario";
    public static String PARAM_RATE = "tauxPeriodique";
    public static String PAYMENT = "versement";
    
    // MonthlyPayment Strings
    public static String PERIOD = "periode";
    public static String PAYMENT_DATE = "date";
    public static String CAPITAL_B = "capitalDebut";
    public static String INTEREST = "interet";
    public static String CAPITAL = "capital";
    public static String CAPITAL_E = "capitalFin";
    public static String CUMULATIVE_TOTAL = "totalCumulatif";
    public static String CUMULATIVE_INTEREST = "interetCumulatif";
    public static String CUMULATIVE_CAPITAL = "capitalCumulatif";

}
