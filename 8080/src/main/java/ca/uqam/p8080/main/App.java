package ca.uqam.p8080.main;

import ca.uqam.p8080.calculation.utils.json.JsonCapsule;
import ca.uqam.p8080.calculation.utils.json.JsonInterface;
import ca.uqam.p8080.calculation.utils.json.Validation;
import ca.uqam.p8080.calculation.Loan;
import java.io.IOException;
import java.text.ParseException;
import org.json.simple.JSONObject;

public class App {
    public static void main(String [] args) throws IOException, ParseException, Exception{
        if(args.length < 1) throw new Exception("Pas assez de parametres.");
        
        JsonInterface jsonInterface = JsonInterface.getInstance();
        JsonCapsule capsule = new JsonCapsule((JSONObject) jsonInterface.parseJson(args[0]));
        
        Validation validator = Validation.getInstance();
        validator.validate(capsule);
       
        Loan loan = new Loan(capsule);
        jsonInterface.outputWriter(loan.toJsonObject(), "output.json");
        
    }
}
